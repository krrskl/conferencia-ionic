import { EditarItemPage } from './../editar-item/editar-item';
import { DetalleItemPage } from './../detalle-item/detalle-item';
import { NuevoItemPage } from './../nuevo-item/nuevo-item';
import { RegistroPage } from './../registro/registro';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Item } from './../../models/item.interface';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items: Array<{titulo: string, descripcion: string}>
  item : Item;
  constructor(public navCtrl: NavController) {
    this.items = [{titulo:'articulo 1', descripcion:'prueba'}];
  }

  cargarLogin(){
    this.navCtrl.push(LoginPage);
  }
  cargarRegistro(){
    this.navCtrl.push(RegistroPage);
  }
  cargarItem(){
    this.navCtrl.push(NuevoItemPage);
  }
  cargarDetalle(item){
    this.navCtrl.push(DetalleItemPage,{titulo: item.titulo, descripcion: item.descripcion});
  }
  cargarEditarItem(){
    this.navCtrl.push(EditarItemPage);
  }

}
